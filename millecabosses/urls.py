from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from application import views


urlpatterns = patterns(
    "",
    url(r"^admin/", include(admin.site.urls)),
    url(r"^$", views.index, name="index"),
    url(
        r"^(?P<page_id>\d+)-(?P<page_alias>\w+)/$",
        views.page_details,
        name="page_details",
    ),
    url(
        r"^produit/(?P<product_id>\d+)-(?P<product_name>\w+)/$",
        views.product_details,
        name="product_details",
    ),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
