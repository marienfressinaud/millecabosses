FROM python:2.7-alpine

ENV APP_ENVIRONMENT=production
ENV LIBRARY_PATH=/lib:/usr/lib

RUN mkdir /app
WORKDIR /app

RUN apk add build-base python-dev py-pip jpeg-dev zlib-dev

COPY requirements.txt .
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY . .

CMD ["gunicorn", "millecabosses.wsgi:application", "-b", "0.0.0.0:8000"]
