from django.contrib import admin

from .models import ProductCategory, Product, Ingredient
from .models import Media
from .models import Page, TextBlock, Link


class TextBlockInline(admin.StackedInline):
    """Stacked Inline View for TextBlock"""

    model = TextBlock
    extra = 1
    ordering = ("order",)


class PageAdmin(admin.ModelAdmin):
    inlines = [TextBlockInline]


class IngredientInline(admin.StackedInline):
    """Stacked Inline View for Ingredient"""

    model = Ingredient
    extra = 4


class ProductAdmin(admin.ModelAdmin):
    inlines = [IngredientInline]


class MediaAdmin(admin.ModelAdmin):
    list_display = ("filename", "type", "url")


admin.site.register(Media, MediaAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductCategory)
admin.site.register(Page, PageAdmin)
admin.site.register(Link)
