from django import template


register = template.Library()


@register.filter
def clean_url(link):
    if not link.startswith("http://") and not link.startswith("https://"):
        return "http://{0}".format(link)

    return link
