from django import template

import unicodedata
import re


register = template.Library()


@register.filter
def simplify(text):
    text = unicodedata.normalize("NFD", text).encode("ascii", "ignore")
    # keep only letters, numbers and _, all the others chars are replaced by _
    text = re.sub(r"[^\w\d_]", "_", text)
    # be sure there are no two or more successive _
    text = re.sub(r"_{2,}", "_", text)
    # don't return a string beginning or ending by _
    return text.strip("_").lower()
