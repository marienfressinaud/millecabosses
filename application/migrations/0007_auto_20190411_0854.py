# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_thumbs.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0006_auto_20190411_0853'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=django_thumbs.db.models.ImageWithThumbsField(default=b'', upload_to=b'', blank=True),
        ),
    ]
