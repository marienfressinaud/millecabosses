# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0007_auto_20190411_0854'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='attachments',
            field=models.ManyToManyField(to='application.Media'),
        ),
    ]
