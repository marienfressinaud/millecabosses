# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import apps as global_apps
from django.db import migrations


def import_pages_app(apps, schema_editor):
    try:
        OldLink = apps.get_model("pages", "Link")
        OldPage = apps.get_model("pages", "Page")
        OldTextBlock = apps.get_model("pages", "TextBlock")
    except LookupError:
        # The old app isn't installed.
        return

    NewLink = apps.get_model("application", "Link")
    NewPage = apps.get_model("application", "Page")
    NewTextBlock = apps.get_model("application", "TextBlock")

    for old_link in OldLink.objects.all():
        NewLink.objects.create(id=old_link.id, title=old_link.title, url=old_link.url)

    for old_page in OldPage.objects.all():
        NewPage.objects.create(
            id=old_page.id,
            title=old_page.title,
            alias=old_page.alias,
            pub_date=old_page.pub_date,
            layout=old_page.layout,
            order=old_page.order,
            location=old_page.location,
        )

    for old_text_block in OldTextBlock.objects.all():
        page = NewPage.objects.get(title=old_text_block.page.title)
        NewTextBlock.objects.create(
            id=old_text_block.id,
            content=old_text_block.content,
            is_important=old_text_block.is_important,
            is_centered=old_text_block.is_centered,
            illustration_url=old_text_block.illustration_url,
            order=old_text_block.order,
            page=page,
        )


class Migration(migrations.Migration):
    operations = [migrations.RunPython(import_pages_app, migrations.RunPython.noop)]

    dependencies = [("application", "0004_link_page_textblock")]

    if global_apps.is_installed("pages"):
        dependencies.append(("pages", "0011_auto_20150425_0931"))
