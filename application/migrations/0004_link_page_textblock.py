# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0003_auto_20190322_0958'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250)),
                ('url', models.CharField(max_length=250)),
            ],
            options={
                'ordering': ['title'],
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250)),
                ('alias', models.CharField(max_length=50)),
                ('pub_date', models.DateTimeField(verbose_name=b'publication date')),
                ('layout', models.CharField(max_length=50)),
                ('order', models.PositiveSmallIntegerField(default=1)),
                ('location', models.PositiveSmallIntegerField(default=1, choices=[(1, b'header'), (2, b'aside'), (3, b'footer')])),
                ('is_published', models.BooleanField(default=True)),
                ('is_product_page', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='TextBlock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(default=b'')),
                ('is_important', models.BooleanField(default=False)),
                ('is_centered', models.BooleanField(default=False)),
                ('illustration_url', models.CharField(max_length=250, blank=True)),
                ('order', models.PositiveSmallIntegerField(default=1)),
                ('page', models.ForeignKey(to='application.Page')),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
