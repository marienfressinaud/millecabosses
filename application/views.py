# coding: utf-8
from datetime import datetime

from django.shortcuts import render, get_object_or_404
from django.http import Http404

from application.models import Page, Link, ProductCategory, Product


CONTACT = {
    "mail": "chocolaterie1000cabosses@gmail.com",
    "tel": "06 82 77 21 67",
    "address": "7 Grande rue<br />23120 Vallière",
    "facebook": "https://www.facebook.com/chocolaterie1000cabosses",
    "osm": '<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=2.027202844619751%2C45.90343342047435%2C2.0451200008392334%2C45.910055254506595&amp;layer=mapnik&amp;marker=45.9067444362303%2C2.036161422729492"></iframe>',
}


def get_current_season():
    current_date = datetime.now()

    start_easter = datetime(current_date.year, 3, 22)
    end_easter = datetime(current_date.year, 4, 25)
    if start_easter <= current_date <= end_easter:
        return "easter"

    halloween = datetime(current_date.year, 10, 31)
    if halloween.date() == current_date.date():
        return "halloween"

    start_christmas = datetime(current_date.year, 12, 1)
    end_christmas = datetime(current_date.year, 12, 31)
    if start_christmas <= current_date <= end_christmas:
        return "christmas"


def get_site_infos(current_page, show_list_products=False, current_product=None):
    return {
        "current_page": current_page,
        "pages_header": Page.objects.filter(location=1, is_published=True),
        "pages_aside": Page.objects.filter(location=2, is_published=True),
        "pages_footer": Page.objects.filter(location=3, is_published=True),
        "product_categories": ProductCategory.objects.all(),
        "list_products_active": show_list_products,
        "links": Link.objects.all(),
        "contact": CONTACT,
        "current_product": current_product,
        "current_season": get_current_season(),
    }


def index(request):
    return page_details(request)


def page_details(request, page_id=None, page_alias=None):
    if page_id is None:
        page = Page.objects.first()
    else:
        page = get_object_or_404(Page, pk=page_id)

    show_list_products = page.is_product_page

    return render(
        request,
        "application/page_details.html",
        get_site_infos(page, show_list_products),
    )


def product_details(request, product_id, product_name=None):
    product = get_object_or_404(Product, pk=product_id)

    # That's a dirty way to set active "Mes produits" item in the header
    pages = Page.objects.filter(is_product_page=True, is_published=True)
    if not pages:
        raise Http404("There is no product page.")

    return render(
        request,
        "application/product_details.html",
        get_site_infos(pages[0], True, product),
    )
