# coding: utf-8

import os

from django.db import models
from django_thumbs.db import models as models_thumb


LOCATION_CHOICES = ((1, "header"), (2, "aside"), (3, "footer"))


class Media(models.Model):
    file = models.FileField()

    def type(self):
        _, ext = os.path.splitext(self.file.name)
        ext = ext[1:].lower()
        if ext == "pdf":
            return "PDF"
        elif ext in ("jpg", "jpeg", "png", "gif"):
            return "Image"
        else:
            return "Autre"

    def filename(self):
        return os.path.basename(os.path.splitext(self.file.name)[0])

    def url(self):
        return self.file.url

    def __unicode__(self):
        return self.filename()


class Page(models.Model):
    title = models.CharField(max_length=250)
    alias = models.CharField(max_length=50)
    pub_date = models.DateTimeField("publication date")
    layout = models.CharField(max_length=50)
    order = models.PositiveSmallIntegerField(default=1)
    location = models.PositiveSmallIntegerField(default=1, choices=LOCATION_CHOICES)
    is_published = models.BooleanField(default=True)
    is_product_page = models.BooleanField(default=False)

    class Meta:
        ordering = ["order"]

    def iter_blocks(self):
        blocks = self.textblock_set.order_by("order").all()
        current_block = 0

        for char in self.layout:
            try:
                nb_block = int(char)
            except ValueError:
                continue

            next_block = current_block + nb_block
            yield blocks[current_block:next_block]
            current_block = next_block

    def __unicode__(self):
        return self.title


class TextBlock(models.Model):
    content = models.TextField(default="")
    is_important = models.BooleanField(default=False)
    is_centered = models.BooleanField(default=False)
    illustration_url = models.CharField(max_length=250, blank=True)
    order = models.PositiveSmallIntegerField(default=1)
    page = models.ForeignKey(Page)

    class Meta:
        ordering = ["order"]
        verbose_name = "Bloc de texte"

    def __unicode__(self):
        return self.content[:100]


class Link(models.Model):
    title = models.CharField(max_length=250)
    url = models.CharField(max_length=250)

    class Meta:
        ordering = ["title"]
        verbose_name = "Lien"

    def __unicode__(self):
        return self.title


class ProductCategory(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        ordering = ["name"]
        verbose_name = "Catégorie de produits"
        verbose_name_plural = "Catégories de produits"

    def __unicode__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField(default="")
    image = models_thumb.ImageWithThumbsField(
        default="", blank=True, sizes=((150, 150),)
    )
    category = models.ForeignKey(ProductCategory)
    attachments = models.ManyToManyField(Media, blank=True)

    def all_images(self):
        if self.image:
            yield self.image

        for media in self.attachments.all():
            if media.type() == "Image":
                yield media

    def other_images(self):
        for media in self.attachments.all():
            if media.type() == "Image":
                yield media

    class Meta:
        ordering = ["name"]
        verbose_name = "Produit"

    def __unicode__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=250)
    origin = models.CharField(max_length=250, blank=True)
    url = models.CharField(max_length=250, blank=True)
    product = models.ForeignKey(Product)

    class Meta:
        ordering = ["name"]
        verbose_name = "Ingrédient"

    def __unicode__(self):
        return self.name
